class ProductsController < ApplicationController

  def index
    @currency = Currency.all
    @products = Product.all
  end

  def show
    @product = Product.find(params[:id])
  end

  def get_exchange_values

    unless params[:id].blank?
      product = Product.find_by(:id => params[:id])
      render json: [] if product.blank?

      @current_value = {:id => product.id , :currency => get_currency_rate(params["currency"],product)}

      render json: [@current_value.compact]

    else
      
      @current_values = Product.all.collect{|product| {:id => product.id , :currency => Product.get_currency_rate(params["currency"],product)} }
      render json: @current_values.compact
    end
  end

  private

  def product_params
    params.require(:product).permit(:name, :description, :price_in_cents)
  end
end
